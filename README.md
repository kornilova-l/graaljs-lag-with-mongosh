`./gradlew run`
 
Expected result:
```
Iteration 1
evaluation of script file: 2602
first invocation of script: 8090
second invocation of script: 212
Iteration 2
evaluation of script file: 149
first invocation of script: 5415
second invocation of script: 120
```

As you can see in both iterations first invocation of the script takes a long time.
