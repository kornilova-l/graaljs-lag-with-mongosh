package kornilova;

import org.apache.commons.io.IOUtils;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Engine;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyExecutable;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class Main {
    public static void main(String[] args) throws IOException {
        Engine engine = Engine.create();
        String text = IOUtils.toString(Main.class.getResourceAsStream("/js/all-standalone.js"), StandardCharsets.UTF_8);
        System.out.println(System.getProperty("java.version"));
        Source source = Source.newBuilder("js", text, "all-standalone.js").build();
        for (int i = 0; i < 2; i++) {
            System.out.println("Iteration " + (i + 1));
            doTest(engine, source);
        }
    }

    private static void doTest(Engine engine, Source source) throws IOException {
        Context context = Context.newBuilder().engine(engine).build();
        measureTime("evaluation of script file", () -> context.eval(source));
        // see full code here: https://github.com/kornilova203/mongosh/blob/master/packages/java-shell/src/main/kotlin/com/mongodb/mongosh/MongoShellEvaluator.kt
        Value global = context.getBindings("js").getMember("_global");
        Value shellInstanceState = global.getMember("ShellInstanceState").newInstance(context.eval("js", "new Object()"));
        Value shellEvaluator = global.getMember("ShellEvaluator").newInstance(shellInstanceState, (ProxyExecutable) arguments -> arguments);
        measureTime("first invocation of script", () -> evaluate("41", context, shellEvaluator));
        measureTime("second invocation of script", () -> evaluate("42", context, shellEvaluator));
    }

    private static void measureTime(String name, Runnable runnable) {
        long time = System.currentTimeMillis();
        runnable.run();
        System.out.println(name + ": " + (System.currentTimeMillis() - time));
    }

    private static void evaluate(String script, Context context, Value shellEvaluator) {
        ProxyExecutable originalEval = arguments -> context.eval("js", arguments[0].asString());
        // customEval definition: https://github.com/mongodb-js/mongosh/blob/f1e6417374a8aba22f299961b992241d7376d2af/packages/shell-evaluator/src/shell-evaluator.ts#L93
        // it performs a number of common babel transformations on script and then executes it
        Value promise = shellEvaluator.invokeMember("customEval", originalEval, script);
        promise.invokeMember("then", (ProxyExecutable) arguments -> {
//            System.out.println("result: " + arguments[0]);
            return null;
        });
    }

}
